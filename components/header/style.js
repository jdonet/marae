 import {StyleSheet} from 'react-native'
 import {APP_COLORS} from '../../style/color.js'

 export const style = StyleSheet.create({
     subHeader:{
        backgroundColor: APP_COLORS.darkPrimary,
        height:30
     },
     header:{
        backgroundColor: APP_COLORS.primary,
        height:50,
        shadowColor:APP_COLORS.shadow,
        shadowOpacity:0.2,
        shadowOffset:{height:10 },
        flexDirection:'row',
        justifyContent:'space-around',
     },
     text:{
        color: APP_COLORS.primaryText,
        fontSize:30
     }
 })