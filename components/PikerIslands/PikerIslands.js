import React from 'react';
import { Text,View,Picker } from 'react-native';
import {style} from './style'


const PikerIslands = ({islands,selectedIsland, onChangeCallback,currentPosition}) =>(
<Picker
selectedValue={typeof(selectedIsland) === 'undefined' ? islands[11] : selectedIsland} //par défaut, on charge Tahiti
onValueChange= {onChangeCallback}
>
  <Picker.Item 
    key={currentPosition?currentPosition.num:0}
    label={currentPosition?currentPosition.ile:"Ma position"} 
    value={currentPosition?currentPosition:null}
      />
 {islands.map((island, i) => (
      <Picker.Item 
      key={island.num}
      label={island.ile} 
      value={island}
       />
  ))}
</Picker>
);

export default PikerIslands;

