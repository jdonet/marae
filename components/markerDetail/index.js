import React from 'react';
import { View,Text } from 'react-native';
import { Button } from 'react-native-elements';
import Modal from 'react-native-modal';
import { style } from './style';

const MarkerDetail = ({markerName,markerDescription,isVisible,onDisapearCallback}) =>(
    <Modal 
        isVisible={isVisible}
        onBackdropPress={() => onDisapearCallback()} 
    >
            <View style={style.modal}>
                <View style={style.textView}>
                    <Text style={style.title}>{markerName}</Text>
                    <Text>{markerDescription}</Text>
                </View>
                <View style={style.buttonView}>
                    <Button 
                        title="Fermer"
                        onPress={()=> onDisapearCallback()}
                        buttonStyle={style.buttonDelete}
                    />
                    
                </View>
            </View>
    </Modal>
        
);

export default MarkerDetail;
