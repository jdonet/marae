import React, { Component } from 'react';
import { View,Text} from 'react-native';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';

const MapScreen = ({initialRegion,region, markers,onPressCallback}) =>(
    <View style={{ flex: 1 }}>
        <MapView
            style={{ flex: 1 }}
            initialRegion={initialRegion}
            region = {region}
            >
            {markers.map((marker,i) => (
                <Marker
                key={marker.id}
                coordinate={marker.coordinate}
                title={marker.title}
                description={marker.description}
                onPress = {() => onPressCallback(marker,i)}
                />
            ))}
        </MapView>
    </View>
    
);
  
export default MapScreen;